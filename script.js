
// collapsable section with logs from storage


const prevSessionLogsEl = document.querySelector(".previous-session-logs")
const currentSessionLogEl = document.querySelector(".current-session-log")
let gKeyNr
window.onload = () => {
    gKeyNr=getNextKeyNumberFn()
    printKeysValuesFromStorage()

    myLogFn("window.onload")
    myLogFn(`navigator.userAgent: ${navigator.userAgent}`)

    if (navigator.storage && navigator.storage.persist) {
        myLogFn("Requesting permission for local storage to be persistent, using navigator.storage.persist(). Please note that sometimes the user may be prompted, and sometimes not")
        navigator.storage.persist().then(iIsPersitent => {
            if (iIsPersitent) {
                myLogFn("Local storage is persistent (in the case of device storage preassure, unclear if more is covered)")
            } else {
                myLogFn("WARNING: Local storage is unfortunately not persistent. This may be because it was disallowed by the user after prompted (or maybe that the system doesn't allow it?)")
            }
        })
    } else {
        myLogFn("ERROR: Unable to even request permission for local storage to be persistent")
    }

    /*
    let db
    const storageNameSg="my-storage"
    if(window.indexedDB){
        myLogFn("This browser has support for indexedDB")
        const dbRequest=window.indexedDB.open("web-test")
        dbRequest.addEventListener("onerror", (iEvent)=>{
            myLogFn(`ERROR: ${iEvent.target.errorCode}`)
        })
        dbRequest.addEventListener("onsuccess", (iEvent)=>{
            db=iEvent.target.result
        })

    }else{
        myLogFn("WARNING: Unfortunately this browser doesn't support indexedDB")
    }

    function getData(iDb, iKeySg){
        const transaction=iDb.transaction(storageNameSg)
        const objectStore=transaction.objectStore(storageNameSg)
        const dbRequest = objectStore.get(iKeySg)
        return dbRequest.result.name
    }

    const readData=getData(db, "test-key")
    myLogFn(readData)
    */
}

window.addEventListener("beforeunload", (iEvent) => {
    // https://developer.mozilla.org/en-US/docs/Web/API/Window/beforeunload_event
    myLogFn("window.beforeunload")
})

function myVariableLog(iVariable) {
    const textSg = `${Object.keys({ iVariable })[0]} = ${iVariable}`
    // Doesn't work: Prints iVariable, not the name of the calling variable. maybe getting the stack trace can help here?
    myLogFn(textSg)
}

function getCurrentDateTextFn() {
    const nowDateTimeOt = new Date()
    const yearSg = nowDateTimeOt.getFullYear().toString()
    const monthSg = (nowDateTimeOt.getMonth() + 1).toString().padStart(2, "0")
    const dateDaySg = nowDateTimeOt.getDate().toString().padStart(2, "0")
    const hoursSg = nowDateTimeOt.getHours().toString().padStart(2, "0")
    const minutesSg = nowDateTimeOt.getMinutes().toString().padStart(2, "0")
    const secondsSg = nowDateTimeOt.getSeconds().toString().padStart(2, "0")
    const nowDateTimeSg = `${yearSg}-${monthSg}-${dateDaySg} ${hoursSg}:${minutesSg}:${secondsSg}`
    return nowDateTimeSg
}
function printKeysValuesFromStorage() {
    keysValuesAr = Array.of()
    let sortedKeyArray = Object.keys(window.localStorage).sort(function (a, b) {
        return window.localStorage[a] - window.localStorage[b]
    })
    for (keySg in sortedKeyArray) {
        const value = window.localStorage.getItem(keySg)
        const logEl = document.createElement("pre")
        prevSessionLogsEl.appendChild(logEl)
        logEl.textContent = value
    }
}
function getNextKeyNumberFn() {
    let highestKeyNr = 0
    for (keySg of Object.keys(window.localStorage)) {
        const keyNr = parseInt(keySg)
        if (highestKeyNr < keyNr) {
            highestKeyNr = keyNr
        }
    }
    const nextKeyTextNr = (highestKeyNr + 1)
    return nextKeyTextNr
}
function myLogFn(iTextSg) {
    const nowDateTimeSg = getCurrentDateTextFn()
    const textWithDateTimeSg = `${nowDateTimeSg} ${iTextSg}`
    const logEl = document.createElement("pre")
    currentSessionLogEl.appendChild(logEl)
    logEl.textContent = textWithDateTimeSg
    console.log(textWithDateTimeSg)

    gKeyNr=gKeyNr+1
    window.localStorage.setItem(gKeyNr.toString(), textWithDateTimeSg)
}
